﻿using DotNetty.Wraper;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading.Tasks;

namespace UdpSocket.Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();
            var theServer = await SocketBuilderFactory.GetUdpSocketBuilder(Examples.Common.ServerSettings.Port)
                .OnChannelRegistered((server, channel) =>
                {
                    _logger.LogDebug("ChannelUnregistered: " + channel.Id);
                })
                .OnChannelRegistered((server, channel) =>
                {
                    _logger.LogDebug("ChannelRegistered: " + channel.Id);
                }).OnClose(server =>
                {
                    _logger.LogInformation($"服务端关闭");
                })
                .OnException(ex =>
                {
                    _logger.LogError($"服务端异常:{ex.Message}");
                })
                .OnRecieve((server, point, bytes) =>
                {
                    _logger.LogInformation($"服务端:收到来自[{point.ToString()}]数据:{Encoding.UTF8.GetString(bytes)}");
                    server.Send(bytes, point);
                })
                .OnSend((server, point, bytes) =>
                {
                    _logger.LogInformation($"服务端发送数据:目标[{point.ToString()}]数据:{Encoding.UTF8.GetString(bytes)}");
                })
                .OnStarted(server =>
                {
                    _logger.LogInformation($"服务端启动");
                }).BuildAsync();

            Console.ReadLine();
        }
    }
}
