﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace TcpSocket.Server
{
    using System;
    using System.Text;
    using CNative.Logging;
    using DotNetty.Buffers;
    using DotNetty.Transport.Channels;
    using DotNetty.Wraper;
    using Microsoft.Extensions.Logging;

    public class EchoServerHandler : BaseSimpleChannelInboundHandler<object>
    {
        public EchoServerHandler(IChannelEvent channelEvent) : base(channelEvent)
        {
        }
        private readonly ILogger _logger = new ConfigLogger();// LoggingHelper.Logger;
        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            var buffer = message as IByteBuffer;
            if (buffer != null)
            {
                //Console.WriteLine("Received from client: " + buffer.ToString(Encoding.UTF8));
                _logger.LogInformation("Received from client: " + buffer.ToString(Encoding.UTF8));
            }
            context.WriteAsync(message);
        }
    }
}