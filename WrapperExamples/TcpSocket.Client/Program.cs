﻿using DotNetty.Wraper;
using Examples.Common;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TcpSocket.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();// LoggingHelper.Logger;
            X509Certificate2 cert = null;
            if (ClientSettings.IsSsl)
            {
                cert = new X509Certificate2(Path.Combine(ExampleHelper.ProcessDirectory, "dotnetty.com.pfx"), "password");
            }

            var theClientBuild = SocketBuilderFactory.GetTcpSocketClientBuilder(new IPEndPoint(Examples.Common.ClientSettings.Host, Examples.Common.ClientSettings.Port));
          
            theClientBuild
                //.AddLast("framing-enc", new DotNetty.Codecs.LengthFieldPrepender(2))
                //.AddLast("framing-dec", new DotNetty.Codecs.LengthFieldBasedFrameDecoder(ushort.MaxValue, 0, 2, 0, 2))
  
                .OnChannelRegistered((client, channel) =>
                {
                    _logger.LogDebug("OnChannelRegistered: " + channel.Id);
                })
                .OnChannelRegistered((client, channel) =>
                {
                    _logger.LogDebug("OnChannelRegistered: " + channel.Id);
                })
                .OnClientStarted(client =>
                {
                    _logger.LogInformation($"客户端启动");
                    client.Send("Hello world" + DateTime.Now);
                })
                .OnClientClose(client =>
                {
                    _logger.LogInformation($"客户端关闭");
                })
                .OnException(ex =>
                {
                    _logger.LogError($"异常:{ex.Message}");
                })
                .OnRecieve((client, bytes) =>
                {
                    _logger.LogInformation($"客户端:收到数据:{Encoding.UTF8.GetString(bytes)}");
                    //client.Send("Hello world" + DateTime.Now);
                })
                .OnSend((client, bytes) =>
                {
                    _logger.LogInformation($"客户端:发送数据:{Encoding.UTF8.GetString(bytes)}");
                });

            var theClient = await theClientBuild .BuildAsync(null, (pipeline) =>
            {
                pipeline.AddLast("framing-enc", new DotNetty.Codecs.LengthFieldPrepender(2));
                pipeline.AddLast("framing-dec", new DotNetty.Codecs.LengthFieldBasedFrameDecoder(ushort.MaxValue, 0, 2, 0, 2));
            }, ClientSettings.UseLibuv, cert);
                //.BuildAsync((tcpCleint) => { return new EchoClientHandler(tcpCleint as IChannelEvent); });

            //while (true)
            //{
            //    await theClient.Send(Guid.NewGuid().ToString());

            //    await Task.Delay(1000);
            //}
            Console.ReadLine();

            theClient?.Close();
        }
    }
}