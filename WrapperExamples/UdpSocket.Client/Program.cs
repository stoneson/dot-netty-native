﻿using DotNetty.Wraper;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UdpSocket.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();
            var ept = new IPEndPoint(Examples.Common.ClientSettings.Host, Examples.Common.ClientSettings.Port);
            var theClient = await SocketBuilderFactory.GetUdpSocketBuilder()
                .OnChannelRegistered((server, channel) =>
                {
                    _logger.LogDebug("ChannelUnregistered: " + channel.Id);
                })
                .OnChannelRegistered((server, channel) =>
                {
                    _logger.LogDebug("ChannelRegistered: " + channel.Id);
                })
                .OnClose(client =>
                {
                    _logger.LogInformation($"客户端关闭");
                })
                .OnException(ex =>
                {
                    _logger.LogError($"客户端异常:{ex.Message}");
                })
                .OnRecieve((client, point, bytes) =>
                {
                    _logger.LogInformation($"客户端:收到来自[{point.ToString()}]数据:{Encoding.UTF8.GetString(bytes)}");
                    client.Send("Hello world" + DateTime.Now, ept);
                })
                .OnSend((client, point, bytes) =>
                {
                    _logger.LogInformation($"客户端发送数据:目标[{point.ToString()}]数据:{Encoding.UTF8.GetString(bytes)}");
                })
                .OnStarted(client =>
                {
                    _logger.LogInformation($"客户端启动");
                    client.Send("Hello world" + DateTime.Now, ept);
                }).BuildAsync();

            //while (true)
            //{
            //    await theClient.Send(Guid.NewGuid().ToString(), ept);
            //    await Task.Delay(1000);
            //}
            Console.ReadLine();
        }
    }
}
