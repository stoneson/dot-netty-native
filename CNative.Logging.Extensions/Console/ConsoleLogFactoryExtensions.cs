﻿
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

#if NET451
#else
using Microsoft.Extensions.Options;
#endif
namespace CNative.Logging
{
    public static class ConsoleLogFactoryExtensions
    {
#if NET451
        public static ILoggerFactory AddConsoleLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new ConsoleLoggerProvider((s, level) => true, false));
            return factory;
        }
#else
        public static ILoggerFactory AddConsoleLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(getILoggerProvider());
            return factory;
        }
        public static ILoggingBuilder AddConsoleLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(getILoggerProvider());
            return builder;
        }

        private static ILoggerProvider getILoggerProvider()
        {
            var configureNamedOptions = new Microsoft.Extensions.Options.ConfigureNamedOptions<ConsoleLoggerOptions>("SetConsoleLogger", null);
            var optionsFactory = new OptionsFactory<ConsoleLoggerOptions>(new[] { configureNamedOptions }, Enumerable.Empty<IPostConfigureOptions<ConsoleLoggerOptions>>());
            var optionsMonitor = new OptionsMonitor<ConsoleLoggerOptions>(optionsFactory, Enumerable.Empty<IOptionsChangeTokenSource<ConsoleLoggerOptions>>(), new OptionsCache<ConsoleLoggerOptions>());
            //InternalLoggerFactory.DefaultFactory.AddProvider();
            return new ConsoleLoggerProvider(optionsMonitor);

        }
#endif
    }
}