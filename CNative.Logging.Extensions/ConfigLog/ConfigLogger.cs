﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace CNative.Logging
{
    public class ConfigLogger : ILogger
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
        /// <summary>
        /// 判断是否开启记录
        /// </summary>
        public bool IsEnabled(LogLevel logLevel)
        {
            return LoggingHelper.IsEnabled(logLevel);
        }


        /// <summary>
        /// 实现接口ILogger
        /// </summary>
        /// <typeparam name="TState"></typeparam>
        /// <param name="logLevel"></param>
        /// <param name="eventId"></param>
        /// <param name="state"></param>
        /// <param name="exception"></param>
        /// <param name="formatter"></param>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            LoggingHelper.Log(logLevel, eventId, state, exception, formatter);
        }

    }
}
