﻿#if NETSTANDARD2_1
using Exceptionless;

using Exceptionless.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using ExceptionlessLogging= Exceptionless.Logging;
namespace CNative.Logging
{
    public static class ExceptionlessFactoryExtensions
    {
        public static ILoggerFactory AddExceptionlessLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(getILoggerProvider());
            return factory;
        }

       

        public static ILoggingBuilder AddExceptionlessLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(getILoggerProvider());
            return builder;
        }
       
        private static ILoggerProvider getILoggerProvider()
        {
            return new ExceptionlessLoggerProvider((cf) =>
            {
                try
                {
                    string isUseExceptionless =AppConfigurtaion.GetAppSettings("Exceptionless:UseExceptionless", "false");
                    if (isUseExceptionless.ToLower() == "true" || isUseExceptionless == "1")
                    {
                        cf.ApiKey = AppConfigurtaion.GetAppSettings("Exceptionless:ApiKey");
                        cf.ServerUrl = AppConfigurtaion.GetAppSettings("Exceptionless:ServerUrl");

                        var logLevel = AppConfigurtaion.GetAppSettings("Logging:LogLevel:Exceptionless.Extensions.Logging.ExceptionlessLogger", "Error");
                        cf.SetDefaultMinLogLevel(ExceptionlessLogging.LogLevel.FromString(logLevel));
                    }
                }
                catch { }
            });
        } 
    }
}

#endif