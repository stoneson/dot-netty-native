﻿#if NETSTANDARD2_1
using CNative.Logging.Exceptionless.NLog;
using Exceptionless;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging
{
    public static class ExceptionlessNLogFactoryExtensions
    {
        public static ILoggerFactory AddExceptionlessNLogLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new ExceptionlessProvider());
            return factory;
        }


        public static ILoggingBuilder AddExceptionlessNLogLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(new ExceptionlessProvider());
            return builder;
        }
    }
}
#endif