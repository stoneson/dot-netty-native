﻿using System;
using System.Net;

namespace DotNetty.Wraper
{
    class UdpSocketEvent
    {
        public Action<IUdpSocket, Transport.Channels.IChannel> OnChannelRegistered { get; set; }
        public Action<IUdpSocket, Transport.Channels.IChannel> OnChannelUnregistered { get; set; }

        public Action<IUdpSocket> OnStarted { get; set; }
        public Action<IUdpSocket, EndPoint, byte[]> OnRecieve { get; set; }
        public Action<IUdpSocket, EndPoint, byte[]> OnSend { get; set; }
        public Action<IUdpSocket> OnClose { get; set; }
        public Action<Exception> OnException { get; set; }
    }
}
