﻿using DotNetty.Transport.Channels;
using System;

namespace DotNetty.Wraper
{
    public interface IChannelEvent
    {
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="context"></param>
        void OnChannelRegistered(IChannel context);
        /// <summary>
        /// 注销
        /// </summary>
        /// <param name="context"></param>
        void OnChannelUnregistered(IChannel context);
        /// <summary>
        /// 上线
        /// </summary>
        /// <param name="ctx"></param>
        void OnChannelActive(IChannelHandlerContext ctx);
        /// <summary>
        /// 离线
        /// </summary>
        /// <param name="channel"></param>
        void OnChannelInactive(IChannel channel);
        /// <summary>
        /// 收到消息
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="msg"></param>
        void OnChannelReceive(IChannelHandlerContext ctx, object msg);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="exception"></param>
        void OnException(IChannel channel, Exception exception);
    }
}