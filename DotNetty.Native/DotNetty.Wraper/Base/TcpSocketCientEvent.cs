﻿using System;

namespace DotNetty.Wraper
{
    class TcpSocketCientEvent<TSocketClient, TData>
    {
        public Action<TSocketClient, Transport.Channels.IChannel> OnChannelRegistered { get; set; }
        public Action<TSocketClient, Transport.Channels.IChannel> OnChannelUnregistered { get; set; }

        public Action<TSocketClient> OnClientStarted { get; set; }
        public Action<TSocketClient, TData> OnRecieve { get; set; }
        public Action<TSocketClient, TData> OnSend { get; set; }
        public Action<TSocketClient> OnClientClose { get; set; }
        public Action<Exception> OnException { get; set; }
    }
}
