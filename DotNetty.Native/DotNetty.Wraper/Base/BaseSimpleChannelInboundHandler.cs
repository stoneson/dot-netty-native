﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace DotNetty.Wraper
{
    using System;
    using System.Text;
    using DotNetty.Buffers;
    using DotNetty.Handlers.Timeout;
    using DotNetty.Transport.Channels;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// 继承 SimpleChannelInboundHandler<I>
    /// </summary>
    /// <typeparam name="I"></typeparam>
    public class BaseSimpleChannelInboundHandler<I> : SimpleChannelInboundHandler<I>//ChannelHandlerAdapter
    {
        protected IChannelEvent _channelEvent { get; }
        public BaseSimpleChannelInboundHandler(IChannelEvent channelEvent) : this(channelEvent, true)
        {
        }
        public BaseSimpleChannelInboundHandler(IChannelEvent channelEvent, bool autoRelease) : base(autoRelease)
        {
            _channelEvent = channelEvent;
        }
        public override void ChannelRegistered(IChannelHandlerContext context)
        {
            //base.ChannelRegistered(context);
            _channelEvent?.OnChannelRegistered(context.Channel);
        }

        public override void ChannelUnregistered(IChannelHandlerContext context)
        {
            //base.ChannelUnregistered(context);
            _channelEvent?.OnChannelUnregistered(context.Channel);
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            //base.ChannelActive(context);
            _channelEvent?.OnChannelActive(context);
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            //base.ChannelInactive(context);
            _channelEvent?.OnChannelInactive(context.Channel);
        }

        //public override void ChannelRead(IChannelHandlerContext context, object message)
        //{
        //    _channelEvent?.OnChannelReceive(context, message);
        //   // base.ChannelRead(context, message);
        //}
        protected override void ChannelRead0(IChannelHandlerContext context, I message)
        {
            //var buffer = msg as IByteBuffer;
            _channelEvent?.OnChannelReceive(context, message);
        }
        public override void ChannelReadComplete(IChannelHandlerContext context)
        {
            context?.Flush();
        }
        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            //base.ExceptionCaught(context, exception);
            _channelEvent?.OnException(context.Channel, exception);
            context?.CloseAsync();
        }
        public override void UserEventTriggered(IChannelHandlerContext context, object evt)
        {
            if (evt is IdleStateEvent)
            {
                var eventState = evt as IdleStateEvent;
                if (eventState != null)
                {
                    _channelEvent?.OnChannelInactive(context.Channel);
                    context.Channel.CloseAsync();
                }
            }
        }
    }
}