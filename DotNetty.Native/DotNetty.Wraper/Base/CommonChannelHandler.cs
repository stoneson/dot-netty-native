﻿using DotNetty.Transport.Channels;
using System;

namespace DotNetty.Wraper
{
    class CommonChannelHandler : BaseSimpleChannelInboundHandler<object>
    {
        public CommonChannelHandler(IChannelEvent channelEvent) : base(channelEvent)
        {
        }
    }
}