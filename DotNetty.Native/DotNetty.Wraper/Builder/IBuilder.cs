﻿
using System;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    /// <summary>
    /// 构建者
    /// </summary>
    /// <typeparam name="TBuilder">特定构建者</typeparam>
    /// <typeparam name="TTarget">目标生成类</typeparam>
    public interface IBuilder<TBuilder, TTarget>
    {
        /// <summary>
        /// 异常处理
        /// </summary>
        /// <param name="action">异常处理委托</param>
        /// <returns></returns>
        TBuilder OnException(Action<Exception> action);

        /// <summary>
        /// 构建目标生成类
        /// </summary>
        /// <returns></returns>
        Task<TTarget> BuildAsync(Func<TTarget, Transport.Channels.IChannelHandler> addChannelHandler = null, Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null, bool useLibuv = false, System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate=null);

    }
}
