﻿using System;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    abstract class BaseBuilder<TBuilder, TTarget> : IBuilder<TBuilder, TTarget> where TBuilder : class
    {
        public BaseBuilder(int port)
        {
            _port = port;
        }
        protected int _port { get; }

        public abstract Task<TTarget> BuildAsync(Func<TTarget, Transport.Channels.IChannelHandler> addChannelHandler = null, Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null
            , bool useLibuv = false, System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate = null);

        public abstract TBuilder OnException(Action<Exception> action);
    }
}
