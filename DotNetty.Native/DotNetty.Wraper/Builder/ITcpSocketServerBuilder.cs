﻿namespace DotNetty.Wraper
{
    using System;
    using DotNetty.Transport.Channels;
    using System.Threading.Tasks;
    /// <summary>
    /// TcpSocket服务端构建者
    /// </summary>
    public interface ITcpSocketServerBuilder :
        IGenericServerBuilder<ITcpSocketServerBuilder, ITcpSocketServer, ITcpSocketConnection, byte[]>,
        ICoderBuilder<ITcpSocketServerBuilder>
    {
        /// <summary>
        /// 构建目标生成类
        /// </summary>
        /// <returns></returns>
        Task<ITcpSocketServer> BuildAsync(Func<ITcpSocketServer, IChannelHandler> ChannelHandler = null, System.Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null
            , bool useLibuv = false, System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate = null, int SoBacklog = 8192,int eventLoopCount = 1);
    }
}
