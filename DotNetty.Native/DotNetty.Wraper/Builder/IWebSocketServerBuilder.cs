﻿using DotNetty.Transport.Channels;

namespace DotNetty.Wraper
{
    /// <summary>
    /// WebSocket服务端构建者
    /// </summary>
    public interface IWebSocketServerBuilder : IGenericServerBuilder<IWebSocketServerBuilder, IWebSocketServer, IWebSocketConnection, string>
    {
        /// <summary>
        /// 构建目标生成类
        /// </summary>
        /// <returns></returns>
        System.Threading.Tasks.Task<IWebSocketServer> BuildAsync(System.Func<IWebSocketServer, IChannelHandler> ChannelHandler = null, System.Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null
            , bool useLibuv = false, System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate = null, int SoBacklog = 8192, int eventLoopCount = 1);
    }
}
