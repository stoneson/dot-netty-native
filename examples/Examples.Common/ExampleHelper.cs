﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace Examples.Common
{
    using System;
    using System.Linq;
    using DotNetty.Common.Internal.Logging;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging.Console;

#if NET451
#else
    using Microsoft.Extensions.Options;
#endif
    public static class ExampleHelper
    {
        static ExampleHelper()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(ProcessDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public static string ProcessDirectory
        {
            get
            {
#if NET451
                return AppDomain.CurrentDomain.BaseDirectory;
#else
                return AppContext.BaseDirectory;
#endif
            }
        }

        public static IConfigurationRoot Configuration { get; }

        public static void SetConsoleLogger()
        {
#if NET451
         //   InternalLoggerFactory.DefaultFactory.AddProvider(new ConsoleLoggerProvider((s, level) => true, false));
          InternalLoggerFactory.DefaultFactory.AddProvider(new CNative.Logging.ConfigLoggerProvider());
#else

            //var configureNamedOptions = new ConfigureNamedOptions<ConsoleLoggerOptions>("SetConsoleLogger", null);
            //var optionsFactory = new OptionsFactory<ConsoleLoggerOptions>(new[] { configureNamedOptions }, Enumerable.Empty<IPostConfigureOptions<ConsoleLoggerOptions>>());
            //var optionsMonitor = new OptionsMonitor<ConsoleLoggerOptions>(optionsFactory, Enumerable.Empty<IOptionsChangeTokenSource<ConsoleLoggerOptions>>(), new OptionsCache<ConsoleLoggerOptions>());
            //InternalLoggerFactory.DefaultFactory.AddProvider(new ConsoleLoggerProvider(optionsMonitor));

            //InternalLoggerFactory.DefaultFactory.AddProvider(new EventSourceLoggerProvider()); 

            InternalLoggerFactory.DefaultFactory.AddProvider(new CNative.Logging.ConfigLoggerProvider());

#endif
        }
    }
}